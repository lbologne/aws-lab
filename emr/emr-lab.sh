# What you do in this lab:
# - Read S3 data with Hive,
# - Save the output of a Hive query to S3,
# - Join two external tables in Hive,
# - Access Hive tables with Spark SQL,
# - Access Hive tables with pig using HCatalog,
# - Create a simple HBase table and access th e data with Hive.
#
# README:
# - Run this script on an EMR cluster with HBase, Hive, Zookeper, HCatalog, Pig and Spark-SQL installed.
# - You need to generate the orders and lineitem sample data with TPC-H data executing generate-test-data.sh.
# - Replace the placeholder <YOUR BUCKET> whenever required.




#Connect to the Hive shell
hive

# Create the 'orders' external table - Replace <YOUR BUCKET>
create external table orders 
(O_ORDERKEY INT, 
O_CUSTKEY INT, 
O_ORDERSTATUS STRING, 
O_TOTALPRICE DOUBLE, 
O_ORDERDATE STRING, 
O_ORDERPRIORITY STRING, 
O_CLERK STRING, 
O_SHIPPRIORITY INT, 
O_COMMENT STRING) 
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' 
LOCATION 's3://<YOUR BUCKET>/emrdata/';

# Create the 'lineitem' external table - Replace <YOUR BUCKET>
create external table lineitem (
L_ORDERKEY INT,
L_PARTKEY INT,
L_SUPPKEY INT,
L_LINENUMBER INT,
L_QUANTITY INT,
L_EXTENDEDPRICE DOUBLE,
L_DISCOUNT DOUBLE,
L_TAX DOUBLE,
L_RETURNFLAG STRING,
L_LINESTATUS STRING,
L_SHIPDATE STRING,
L_COMMITDATE STRING,
L_RECEIPTDATE STRING,
L_SHIPINSTRUCT STRING,
L_SHIPMODE STRING, L_COMMENT STRING)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
LOCATION 's3://<YOUR BUCKET>/emrdata/';

# Calculate the total discounts of all time and save the result in your S3 bucket
INSERT OVERWRITE DIRECTORY 's3://<YOUR-BUCKET>/output/' select sum(l_discount) from lineitem;

# Join the orders and lineitem tables
select 
  l_shipmode,
  sum(case
    when o_orderpriority ='1-URGENT'
         or o_orderpriority ='2-HIGH'
    then 1
    else 0
end
  ) as high_line_count,
  sum(case
    when o_orderpriority <> '1-URGENT'
         and o_orderpriority <> '2-HIGH'
    then 1
    else 0
end
  ) as low_line_count
from
  orders o join lineitem l 
  on 
    o.o_orderkey = l.l_orderkey and l.l_commitdate < l.l_receiptdate
and l.l_shipdate < l.l_commitdate and l.l_receiptdate >= '1994-01-01' 
and l.l_receiptdate < '1995-01-01'
where 
  l.l_shipmode = 'MAIL' or l.l_shipmode = 'SHIP'
group by l_shipmode
order by l_shipmode;

# Exit the hive shell
exit;




#
# SPARK - HIVE INTEGRATION
#

# Spark SQL setting (Turn off verbose logging):
sudo sed -i -e 's/rootCategory=INFO/rootCategory=WARN/' /etc/spark/conf/log4j.properties

# Launch the Spark SQL console
spark-sql

# Cache the Hive tables in memory
cache table orders;
cache table lineitem;

# You can run the same Hive SQL used above in Spark SQL. Here it will execute significantly faster.
select 
  l_shipmode,
  sum(case
    when o_orderpriority ='1-URGENT'
         or o_orderpriority ='2-HIGH'
    then 1
    else 0
end
  ) as high_line_count,
  sum(case
    when o_orderpriority <> '1-URGENT'
         and o_orderpriority <> '2-HIGH'
    then 1
    else 0
end
  ) as low_line_count
from
  orders o join lineitem l 
  on 
    o.o_orderkey = l.l_orderkey and l.l_commitdate < l.l_receiptdate
and l.l_shipdate < l.l_commitdate and l.l_receiptdate >= '1994-01-01' 
and l.l_receiptdate < '1995-01-01'
where 
  l.l_shipmode = 'MAIL' or l.l_shipmode = 'SHIP'
group by l_shipmode
order by l_shipmode;




#
# HCATALOG - PIG - HIVE INTEGRATION
#
hive

create external table student
(STUDENT STRING,
STUDENT_ID INT)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|';

insert into student values('SANJAY','001');

exit;

pig -useHCatalog

A = LOAD 'student' USING org.apache.hive.hcatalog.pig.HCatLoader(); 

dump A;





#
# HBASE - HIVE INTEGRATION
#
hbase shell

# Create a table named 'team' with one column called 'name'
create 'team', 'descr'

# Inserts one record in the table
put 'team', 'r1', 'descr:name', 'Milan'

# Reads the whole table
scan 'team'

exit

# Read the same table from Hive
hive 

# Creates an external table
set hbase.zookeeper.quorum.<EC2-INSTANCE>;
create external table team_hive (key string, value string)
    stored by 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
    with serdeproperties ("hbase.columns.mapping" = ":key,descr:name")
    tblproperties("hbase.table.name" = "team");

select * from team_hive;



