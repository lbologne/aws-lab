# Use this script to:
# - download the TPC-H toolkit, 
# - generate test data form EMR and RedShift  
# - store the data in your S3 bucket
#
# README:
# 1. SSH into your VM
# ssh -i <PATH-TO-YOUR-PEM> <USER>@<YOUR-EC2-INSTANCE>.<REGION>.compute.amazonaws.com
#
# 2. Install GIT
# sudo yum install -y make git
#
# 3. Clone this file and execute it
# git clone https://<USER>@bitbucket.org/<USER>/aws-lab.git
# chmod 544 aws-lab/big-data-lab/generate-test-data.sh
# ./generate-test-data.sh

export MY_BUCKET = bigdatalabb82 #REPLACE THIS!

git clone https://www.github.com/gregrahn/tpch-kit
cd tpch-kit/dbgen
sudo yum install -y gcc
make OS=LINUX

# Create an S3 bucket to store the test data
aws s3api create-bucket --bucket $MY_BUCKET --region eu-west-1 --create-bucket-configuration LocationConstraint=eu-west-1

# Generate EMR test data: 10 GB of data for the orders and lineitem tables in verbose mode
cd $HOME
mkdir emrdata
export DSS_PATH=$HOME/emrdata
cd tpch-kit/dbgen/
./dbgen -v -T o -s 10

# Move the EMR test data to a bucket
aws s3 cp $HOME/emrdata s3://$MY_BUCKET/emrdata --recursive

# Generate RedShift test data: 40 GB of data for the orders and lineitem tables in verbose mode
cd $HOME
mkdir redshiftdata
export DSS_PATH=$HOME/redshiftdata
cd tpch-kit/dbgen/
./dbgen -v -T o -s 40

# Spits the orders table in 4 files of 15M lines each.
cd $HOME/redshiftdata
split -d -l 15000000 -a 4 orders.tbl orders.tbl.
split -d -l 60000000 -a 4 lineitem.tbl lineitem.tbl.
rm orders.tbl
rm lineitem.tbl

# Move the RedShift test data to a bucket
aws s3 cp $HOME/redshiftdata s3://$MY_BUCKET/redshiftdata --recursive



