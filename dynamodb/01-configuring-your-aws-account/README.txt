The LabMachine and LOAdmin JSON files are CloudFormation templates that generate the user and EC2 instance to use in these labs

The _retry file is used to allow unlimited retries for aws requests. They only stop when you stop them.
It must be copied into your personal hidden aws folder.

cd ~
cd .aws
mkdir model
cp <path>/_retry.json .
